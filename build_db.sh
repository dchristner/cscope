#!/bin/bash

# builds a cscope database for use vim

# just .c and .h
#find . -name "*.[ch]" -print > cscope.files

# .cpp, .c, .h and .hpp
find . -regex ".+\.\(cpp\|c\|h\|hpp\)$" > cscope.files
find . -regex ".+\.\(CPP\|C\|H\|HPP\)$" >> cscope.files

# macosx version
#find -E . -regex ".+\.(cpp|c|h|hpp)$" > cscope.files
#find -E . -regex ".+\.(CPP|C|H|HPP)$" >> cscope.files

# linux version
find -type f -iregex '.*/.*\.\(c\|cpp\|h\|hpp\|xml\)$' | xargs ctags -a

# how to do it
# :'<,'>s/\*\./\*\/\*\./gc

# mac version
#ctags -a  *.cpp  *.hpp 


#cscope -q -k -b -i cscope.files
echo cscope -q -i cscope.files 

cscope  -R -ssrc -sbuild -sbuild_utils

